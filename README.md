# twitch_api_client_vencabot
## Description
This module provides a simple Python interface for interacting with the Twitch website (as opposed to the Twitch chat, which works using IRC).

## Status
This module is very limited and you can see its current capabilities in vencabot_twitch_api_client/endpoints.py .

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
