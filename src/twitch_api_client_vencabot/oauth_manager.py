from datetime import datetime

from .endpoints import refresh_access_token

class TwitchOauthManager:
    def __init__(
            self, client_id: str, client_secret: str,
            refresh_token: str) -> None:
        self.client_id = client_id
        self.client_secret = client_secret
        self.refresh_token = refresh_token
        self.access_token = None
        self.refreshed_at = None
        self.expires_in = None
        self.scope = None
        self.token_type = None

    def refresh(self):
        refreshed_token_data = refresh_access_token(
                self.client_id, self.client_secret, self.refresh_token)
        self.refresh_token = refreshed_token_data["refresh_token"]
        self.access_token = refreshed_token_data["access_token"]
        self.expires_in = refreshed_token_data["expires_in"]
        self.scope = refreshed_token_data["scope"]
        self.token_type = refreshed_token_data["token_type"]
        self.refreshed_at = datetime.now()
