import dataclasses
import datetime
import urllib

from typing import List, Optional, Tuple, Union


API_ENDPOINT_REFERENCE_ANCHOR_MAP = {
        "schedule": "get-channel-stream-schedule",
        "streams": "get-streams",
        "users": "get-users"}
API_REFERENCE_URL = "https://dev.twitch.tv/docs/api/reference/"
API_URL = "https://api.twitch.tv/helix/"
TWITCH_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


class InvalidRefreshTokenError(Exception):
    pass


class TwitchHTTPError(Exception):
    def __init__(
            self, http_error: urllib.error.HTTPError, endpoint: str,
            query_parameters: List[Tuple[str, Union[int, str]]]) -> None:
        self.endpoint = endpoint
        self.query_parameters = query_parameters
        self.code = http_error.code
        self.headers = http_error.headers
        self.reason = http_error.reason
        super().__init__()

    def __str__(self):
        encoded_query = urllib.parse.urlencode(self.query_parameters)
        api_url_str = f"{API_URL}{self.endpoint}?{encoded_query}"
        try:
            anchor = f"#{API_ENDPOINT_REFERENCE_ANCHOR_MAP[self.endpoint]}"
        except KeyError:
            anchor = None
        reference_url_str = f"{API_REFERENCE_URL}{anchor}"
        return (
                f"Request to {api_url_str} failed with HTTP error "
                f"{self.code}. More details at {reference_url_str} .")


@dataclasses.dataclass
class TwitchScheduleSegment:
    segment_id: str
    start_time: datetime.datetime
    end_time: datetime.datetime
    title: str
    canceled_until: Optional[datetime.datetime]
    category_id: int
    category_name: str
    is_recurring: bool


@dataclasses.dataclass
class TwitchStreamData:
    stream_id: int
    user_id: int
    user_login: str
    user_name: str
    game_id: int
    game_name: str
    stream_type: str
    title: str
    tags: List[str]
    viewer_count: int
    started_at: datetime.datetime
    language: str
    thumbnail_url: str
    is_mature: bool


@dataclasses.dataclass
class TwitchUser:
    user_id: int
    login: str
    display_name: str
    user_type: str
    broadcaster_type: str
    description: str
    profile_image_url: str
    offline_image_url: str
    view_count: int
    created_at: datetime.datetime


def _construct_schedule_segment_from_raw_dict(
        segment_data: dict) -> TwitchScheduleSegment:
    segment_data["segment_id"] = segment_data["id"]
    del segment_data["id"]
    segment_data["start_time"] = datetime.datetime.strptime(
            segment_data["start_time"], TWITCH_DATETIME_FORMAT)
    segment_data["end_time"] = datetime.datetime.strptime(
            segment_data["end_time"], TWITCH_DATETIME_FORMAT)
    segment_data["category_id"] = segment_data["category"]["id"]
    segment_data["category_name"] = segment_data["category"]["name"]
    del segment_data["category"]
    if segment_data["canceled_until"] is not None:
        segment_data["canceled_until"] = datetime.datetime.strptime(
                segment_data["canceled_until"], TWITCH_DATETIME_FORMAT)
    return TwitchScheduleSegment(**segment_data)


def _construct_stream_data_from_raw_dict(
        stream_data: dict) -> TwitchStreamData:
    stream_data["stream_id"] = int(stream_data["id"])
    del stream_data["id"]
    stream_data["user_id"] = int(stream_data["user_id"])
    stream_data["game_id"] = int(stream_data["game_id"])
    stream_data["stream_type"] = stream_data["type"]
    del stream_data["type"]
    stream_data["started_at"] = datetime.datetime.strptime(
            stream_data["started_at"], TWITCH_DATETIME_FORMAT)
    # tag_ids is deprecated
    try:
        del stream_data["tag_ids"]
    except KeyError:
        pass
    return TwitchStreamData(**stream_data)
 

def _construct_streams_query_parameters(
        user_ids: List[int], user_logins: List[str],
        game_ids: List[int], stream_type: Optional[str],
        language: Optional[str], page_size: Optional[int]) -> List[tuple]:
    query_parameters = []
    query_parameters += [("user_id", x) for x in user_ids]
    query_parameters += [("user_login", x) for x in user_logins]
    query_parameters += [("game_id", x) for x in game_ids]
    if stream_type is not None:
        query_parameters.append(("type", stream_type))
    if language is not None:
        query_parameters.append(("language", language))
    if page_size is not None:
        query_parameters.append(("first", page_size))
    return query_parameters


def _construct_user_from_raw_dict(user_data: dict) -> TwitchUser:
    user_data["user_id"] = int(user_data["id"])
    del user_data["id"]
    user_data["user_type"] = user_data["type"]
    del user_data["type"]
    user_data["created_at"] = datetime.datetime.strptime(
            user_data["created_at"], TWITCH_DATETIME_FORMAT)
    return TwitchUser(**user_data)
